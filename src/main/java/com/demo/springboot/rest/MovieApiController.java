package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.*;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class MovieApiController {

    @Autowired
    private MovieService movieService;

    @RequestMapping(method = RequestMethod.GET,value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {
        return movieService.getMovies();
    }

    @RequestMapping(method = RequestMethod.POST,value = "/movies")
    public ResponseEntity createNewMovie(@RequestBody MovieDto movie){
        return movieService.createNewMovie(movie);
    }

}
