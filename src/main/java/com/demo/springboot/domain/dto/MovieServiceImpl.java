package com.demo.springboot.domain.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public ResponseEntity createNewMovie(@RequestBody MovieDto movie){

        BufferedWriter writer=null;
        try{
            writer= new BufferedWriter(new FileWriter(movieRepository.csvFile, true));
            for (MovieDto tempmovie:movieRepository.LoadMovies()) {
                if(tempmovie.getMovieId()==movie.getMovieId()){
                    return new ResponseEntity<>(HttpStatus.CONFLICT);
                }
            }
            CSVUtils.writeLine(writer, movie.toListOfString());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    public MovieListDto getMovies() {
        return new MovieListDto(movieRepository.LoadMovies());
    }

}
