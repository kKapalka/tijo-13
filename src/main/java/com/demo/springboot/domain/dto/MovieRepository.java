package com.demo.springboot.domain.dto;

import com.demo.springboot.rest.MovieApiController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public interface MovieRepository {

   String csvFile = "C:\\Users\\student\\Downloads\\movies\\movies\\movies.csv";
    Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    List<MovieDto> LoadMovies();

}
