package com.demo.springboot.domain.dto;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface MovieService {

    MovieListDto getMovies();
    ResponseEntity createNewMovie(@RequestBody MovieDto movie);

}
