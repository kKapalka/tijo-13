package com.demo.springboot.domain.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MovieDto implements Serializable {

    int movieId;
    String title;
    int year;
    String image;

    public MovieDto(int movieId, String title, int year, String image) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }
    public MovieDto() {
    }
    public int getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }
    public List<String> toListOfString(){
        ArrayList<String> movie = new ArrayList<>();
        movie.add(Integer.toString(getMovieId()));
        movie.add(getTitle());
        movie.add(Integer.toString(getYear()));
        movie.add(getImage());
        return movie;
    }
}
