package com.demo.springboot.domain.dto;

import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository

public class MovieRepositoryImpl implements MovieRepository {

@Override
public List<MovieDto> LoadMovies(){
    LOGGER.info("--- get movies");
    List<MovieDto> movies;
    movies = new ArrayList<>();

    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ";";

    try {
        br = new BufferedReader(new FileReader(csvFile));
        br.readLine();
        while ((line = br.readLine()) != null) {
            String[] moviedata = line.split(cvsSplitBy);
            movies.add(new MovieDto(Integer.parseInt(moviedata[0].trim()),moviedata[1],Integer.parseInt(moviedata[2].trim()),moviedata[3]));
        }

    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    return movies;
}
}
